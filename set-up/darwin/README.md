# Darwin

******************
- VersionOS: Mac Os Ventura 13.1 (22C65)
- For more setups visit [](set-up/darwin/extraconf.md)
******************
 My personal script after buy or reset MacOS
 
 To use this script copy these command to Terminal:

 1. Download
```
curl https://gitlab.com/samad20/afterBuyMacOS/-/raw/main/afterBuyMacOS.sh --output afterBuyMacOS.sh
```
 2. edit it if you want, then execute:
```
chmod +x afterBuyMacOS.sh
```
 3. Run
```
./afterBuyMacOS.sh
```

###### tags: `Templates` `Book`
