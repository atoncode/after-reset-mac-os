#! /bin/bash

echo -ne "

░█████╗░███████╗████████╗███████╗██████╗░  ██████╗░██╗░░░██╗██╗░░░██╗  ███╗░░░███╗░█████╗░░█████╗░░█████╗░░██████╗
██╔══██╗██╔════╝╚══██╔══╝██╔════╝██╔══██╗  ██╔══██╗██║░░░██║╚██╗░██╔╝  ████╗░████║██╔══██╗██╔══██╗██╔══██╗██╔════╝
███████║█████╗░░░░░██║░░░█████╗░░██████╔╝  ██████╦╝██║░░░██║░╚████╔╝░  ██╔████╔██║███████║██║░░╚═╝██║░░██║╚█████╗░
██╔══██║██╔══╝░░░░░██║░░░██╔══╝░░██╔══██╗  ██╔══██╗██║░░░██║░░╚██╔╝░░  ██║╚██╔╝██║██╔══██║██║░░██╗██║░░██║░╚═══██╗
██║░░██║██║░░░░░░░░██║░░░███████╗██║░░██║  ██████╦╝╚██████╔╝░░░██║░░░  ██║░╚═╝░██║██║░░██║╚█████╔╝╚█████╔╝██████╔╝
╚═╝░░╚═╝╚═╝░░░░░░░░╚═╝░░░╚══════╝╚═╝░░╚═╝  ╚═════╝░░╚═════╝░░░░╚═╝░░░  ╚═╝░░░░░╚═╝╚═╝░░╚═╝░╚════╝░░╚════╝░╚═════╝░
"


# install Homebrew-The Missing Package Manager for macOS (or Linux)
echo "installing brew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Fish completion for brew-cask
echo "Installing cask..."
brew install brew-cask-completion

echo "update brew"
brew update

# list for what you want install from brew.sh
DEV=(
    # Mac App Store command-line interface
    mas
    # GNU File, Shell, and Text utilities
    coreutils
    # GNU implementation of the famous stream editor
    gnu-sed
    # GNU version of the tar archiving utility
    gnu-tar
    # C code prettifier
    gnu-indent
    # GNU implementation of which utility
    gnu-which
    # GNU grep, egrep and fgrep
    grep
    # Internet file retriever
    wget
    # Bourne-Again SHell, a UNIX command interpreter
    bash
    # GNU compiler collection
    gcc
    # CPU architecture fetching tool
    cpufetch
    # Fast, highly customisable system info script
    neofetch
    # wireguard
    wireguard-tools
    # wireguard
    wireguard-go
    # docker
    docker
    # docker-compose
    docker-compose
    # Graphical network analyzer and capture tool
    wireshark
    # Android palatform tools
    android-platform-tools
    # scrcpy Display and control your Android device
    scrcpy
    # X11 implementation of the Remote Desktop Protocol (RDP)
    freerdp

    android-file-transfer

    syncthing

    glances

    lazygit

    spotify
    
    go
)

# install from brew.sh using DEV list
echo -n "Installing Gnu and dev packages..."
brew install ${DEV[@]}


# list for what you want install from brew CASK
CASKS=(
    brave-browser
    discord
    visual-studio-code
    github
    authy
    #app help you to show shortcuts when hold command button for 5 sec
    cheatsheet
    #to uninstall app totally
    appcleaner
    #Hide menu bar icons to give your Mac a cleaner look
    dozer
    #Move and resize windows in macOS using keyboard shortcuts or snap areas
    rectangle
    # Tool to flash OS images to SD cards & USB drives
    balenaetcher
    # gpg-suite
    gpg-suite
    # OBS Studio  Open-source software for live streaming and screen recording
    obs
    # vlc
    vlc
    # alacritty terminal
    alacritty
    # WM   amethyst
    amethyst
)

# install from brew.sh using CASK list
echo -n "Installing cask apps..."
brew install --cask ${CASKS[@]}


# mas is Mac App Store command-line interface
# mas Available commands:

#    account     Prints the primary account Apple ID
#    install     Install from the Mac App Store
#    list        Lists apps from the Mac App Store which are currently installed
#    outdated    Lists pending updates from the Mac App Store
#    purchase    Purchase and download free apps from the Mac App Store
#    search      Search for apps from the Mac App Store
#    signin      Sign in to the Mac App Store
#    signout     Sign out of the Mac App Store
#    uninstall   Uninstall app installed from the Mac App Store

MAS=(
   # Sunset Code-simple text editor
   1480145554
   # Telegram
   747648890
   # Blackmagic Disk Speed Test
   425264550  
   # bitwarden
   1352778147
   # microsoft remote desktop
   1295203466

)
# install from app store using MAS list
echo -n "Installing apps from App Store..."
mas install ${MAS[@]}


# Minimize animation effect
# Set to scale
defaults write com.apple.dock "mineffect" -string "scale" && killall Dock

#auto-hide the dock and remove its delay on Mac
defaults write com.apple.dock autohide -bool true && defaults write com.apple.dock autohide-delay -float 0 && defaults write com.apple.dock autohide-time-modifier -float 0 && killall Dock

# change dock POSITION
defaults write com.apple.Dock orientation -string down
killall Dock

# change dock SIZE
defaults write com.apple.dock tilesize -int 48

# change dock MAGNIFICATION
defaults write com.apple.dock magnification -bool true

# change dock MAGNIFICATION SIZE
defaults write com.apple.dock largesize -int 64

# change dock MAGNIFICATION ANIMATION DURATION
defaults write com.apple.dock largesize -int 0.1

# Install oh-my-zsh
echo "Installing oh-my-zsh..."
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Install oh-my-zsh plugins
echo "Installing oh-my-zsh plugins..."
#git clone

# Install alacritty yml
echo "Install alacritty yml..."
#git clone
echo -ne "

░██████╗░░█████╗░██████╗░██████╗░██╗░░░██╗███████╗██╗
██╔════╝░██╔══██╗██╔══██╗██╔══██╗╚██╗░██╔╝██╔════╝██║
██║░░██╗░██║░░██║██║░░██║██████╦╝░╚████╔╝░█████╗░░██║
██║░░╚██╗██║░░██║██║░░██║██╔══██╗░░╚██╔╝░░██╔══╝░░╚═╝
╚██████╔╝╚█████╔╝██████╔╝██████╦╝░░░██║░░░███████╗██╗
░╚═════╝░░╚════╝░╚═════╝░╚═════╝░░░░╚═╝░░░╚══════╝╚═╝
"